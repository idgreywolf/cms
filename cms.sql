-- MySQL dump 10.13  Distrib 5.6.25, for Win32 (x86)
--
-- Host: localhost    Database: cms
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attributetypes`
--

DROP TABLE IF EXISTS `attributetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributetypes`
--

LOCK TABLES `attributetypes` WRITE;
/*!40000 ALTER TABLE `attributetypes` DISABLE KEYS */;
INSERT INTO `attributetypes` VALUES (1,'width'),(2,'height'),(3,'margin'),(4,'padding'),(5,'background-image'),(6,'background-position'),(7,'background-repeat'),(8,'vertical-align'),(9,'float'),(10,'box-shadow'),(11,'color'),(12,'font-family'),(13,'font-weight'),(14,'font-size'),(15,'background-color'),(16,'text-align'),(17,'text-shadow');
/*!40000 ALTER TABLE `attributetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_type` int(11) NOT NULL,
  `styleclass` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `hierarchy_id` int(11) NOT NULL,
  `params` longtext,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `block_type` (`block_type`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `blocks_ibfk_1` FOREIGN KEY (`block_type`) REFERENCES `blocktypes` (`id`),
  CONSTRAINT `blocks_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocks`
--

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;
INSERT INTO `blocks` VALUES (1,1,'4',NULL,1,NULL,1),(2,1,'5',1,1,NULL,1),(3,1,'7',1,2,NULL,1),(4,3,'3',3,1,NULL,1),(5,4,NULL,3,2,'href=\"http://www.ozon.ru/context/detail/id/5088316/\"',1);
/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocktypes`
--

DROP TABLE IF EXISTS `blocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `html_tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocktypes`
--

LOCK TABLES `blocktypes` WRITE;
/*!40000 ALTER TABLE `blocktypes` DISABLE KEYS */;
INSERT INTO `blocktypes` VALUES (1,'div'),(2,'h1'),(3,'h2'),(4,'a');
/*!40000 ALTER TABLE `blocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calssesatributes`
--

DROP TABLE IF EXISTS `calssesatributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calssesatributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `class_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calssesatributes`
--

LOCK TABLES `calssesatributes` WRITE;
/*!40000 ALTER TABLE `calssesatributes` DISABLE KEYS */;
INSERT INTO `calssesatributes` VALUES (1,5,'url(\"../pics/background.jpg\")',1),(2,7,'repeat',1),(3,6,'center top',1),(4,15,'#CC6600',1),(5,11,'#990000',2),(6,12,'Times New Roman,Georgia, serif',2),(7,16,'center',2),(8,14,'60px',2),(9,17,'1px 1px 0 #CC6600,	2px 2px 0 #CC6600, 3px 3px 0 #444,	4px 4px 0 #444',2),(10,11,'#282828',3),(12,12,'Times New Roman,Georgia, serif',3),(13,16,'left',3),(14,14,'16px',3),(15,1,'900px',4),(16,2,'380px',4),(17,3,'0px auto',4),(18,4,'10px',4),(19,5,'url(\"../pics/shelf.png\")',4),(20,6,'center bottom',4),(21,7,'no-repeat',4),(22,1,'208px',5),(23,2,'360px',5),(24,9,'left',5),(25,5,'url(\"../pics/lamp.png\")',5),(26,6,'center top',5),(27,7,'no-repeat',5),(28,8,'bottom',5),(29,1,'200px',6),(30,2,'300px',6),(31,10,'7px 7px 7px black',6),(32,8,'bottom',6),(33,1,'688px',7),(34,9,'right',7),(35,11,'black',8),(36,12,'Arial,Verdana, sans-serif',8),(37,13,'bold',8),(38,14,'12px',8);
/*!40000 ALTER TABLE `calssesatributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classesattributes`
--

DROP TABLE IF EXISTS `classesattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classesattributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `class_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `classesattributes_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `attributetypes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classesattributes`
--

LOCK TABLES `classesattributes` WRITE;
/*!40000 ALTER TABLE `classesattributes` DISABLE KEYS */;
INSERT INTO `classesattributes` VALUES (1,5,'url(\"../pics/background.jpg\")',1),(2,7,'repeat',1),(3,6,'center top',1),(4,15,'#CC6600',1),(5,11,'#990000',2),(6,12,'Times New Roman,Georgia, serif',2),(7,16,'center',2),(8,14,'60px',2),(9,17,'1px 1px 0 #CC6600,	2px 2px 0 #CC6600, 3px 3px 0 #444,	4px 4px 0 #444',2),(10,11,'#282828',3),(12,12,'Times New Roman,Georgia, serif',3),(13,16,'left',3),(14,14,'16px',3),(15,1,'900px',4),(16,2,'380px',4),(17,3,'0px auto',4),(18,4,'10px',4),(19,5,'url(\"../pics/shelf.png\")',4),(20,6,'center bottom',4),(21,7,'no-repeat',4),(22,1,'208px',5),(23,2,'360px',5),(24,9,'left',5),(25,5,'url(\"../pics/lamp.png\")',5),(26,6,'center top',5),(27,7,'no-repeat',5),(28,8,'bottom',5),(29,1,'200px',6),(30,2,'300px',6),(31,10,'7px 7px 7px black',6),(32,8,'bottom',6),(33,1,'688px',7),(34,9,'right',7),(35,11,'black',8),(36,12,'Arial,Verdana, sans-serif',8),(37,13,'bold',8),(38,14,'12px',8);
/*!40000 ALTER TABLE `classesattributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `content_type` int(11) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `page_id` (`page_id`),
  KEY `block_id` (`block_id`),
  KEY `content_type` (`content_type`),
  CONSTRAINT `content_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `content_ibfk_2` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`),
  CONSTRAINT `content_ibfk_3` FOREIGN KEY (`content_type`) REFERENCES `contenttypes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,1,2,2,'pics/remark.jpg'),(2,1,1,4,1,'<br>Жизнь, когда не жаль ничего, потому что терять, в сущности, уже нечего.\n								<br>Это - любовь на грани обреченности.<br>Это - роскошь на грани разорения.\n								<br>Это - веселье на грани горя и риск на грани гибели.\n								<br>Будущего - нет. Смерть - не слово, а реальность.\n								<br>Жизнь продолжается. Жизнь прекрасна!..'),(3,1,1,5,2,'pics/ozon.png');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenttypes`
--

DROP TABLE IF EXISTS `contenttypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenttypes`
--

LOCK TABLES `contenttypes` WRITE;
/*!40000 ALTER TABLE `contenttypes` DISABLE KEYS */;
INSERT INTO `contenttypes` VALUES (1,'text'),(2,'image');
/*!40000 ALTER TABLE `contenttypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `head` longtext,
  `parent_page` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `style_id` (`style_id`),
  CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `pages_ibfk_2` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,1,1,'      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n      <title>Библиотека</title>',NULL,'/',NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styleclasses`
--

DROP TABLE IF EXISTS `styleclasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styleclasses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `style_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `style_id` (`style_id`),
  CONSTRAINT `styleclasses_ibfk_1` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styleclasses`
--

LOCK TABLES `styleclasses` WRITE;
/*!40000 ALTER TABLE `styleclasses` DISABLE KEYS */;
INSERT INTO `styleclasses` VALUES (1,'body',1),(2,'H1',1),(3,'H2',1),(4,'#shelf',1),(5,'#picture',1),(6,'#picture img',1),(7,'#description',1);
/*!40000 ALTER TABLE `styleclasses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'index_style');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test','test','test','USER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-14 16:37:04
