package com.cms.java.config;

import com.cms.java.domains.UsersEntity;
import com.cms.java.repositories.interfaces.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SpringSecurityConfig  extends WebSecurityConfigurerAdapter {
    @Autowired
    UsersDAO usersDAO;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        for (UsersEntity userEntity:usersDAO.getAllUsers()) {
            auth.inMemoryAuthentication().withUser(userEntity.getLogin()).password(userEntity.getPassword()).roles(userEntity.getRole());
        }
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/*").access("hasRole('ROLE_USER')")
                .and().formLogin();

    }

}
