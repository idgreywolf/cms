package com.cms.java.controllers;

import com.cms.java.services.PagesHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


@Controller
@RequestMapping("/")
public class IndexController {
	@Autowired
	private PagesHandler pagesHandler;



	@RequestMapping
	public  String  printWelcome(HttpServletRequest request,ModelMap model,Principal principal) {
		String address= (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

		model.addAttribute("head", pagesHandler.getHead(principal.getName(),address));
		model.addAttribute("body", pagesHandler.getBody(principal.getName(),address));

		return "index";
	}
}