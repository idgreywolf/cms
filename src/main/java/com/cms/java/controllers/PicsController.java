package com.cms.java.controllers;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;


@Controller
@RequestMapping("/pics")
public class PicsController {
    @Autowired
    ServletContext servletContext;

    @RequestMapping("*")
    public @ResponseBody  byte[] pics(HttpServletRequest request) throws IOException{
        String filename = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        InputStream in = servletContext.getResourceAsStream("/WEB-INF"+filename);
        return IOUtils.toByteArray(in);
    }
}
