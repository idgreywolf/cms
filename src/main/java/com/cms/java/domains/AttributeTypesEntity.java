package com.cms.java.domains;


import javax.persistence.*;


@Entity
@Table(name="attributetypes")
public class AttributeTypesEntity {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
