package com.cms.java.domains;

import javax.persistence.*;

@Entity
@Table(name="blocks")
public class BlockEntity {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private Integer block_type,parent_id,hierarchy_id,page_id,styleclass;
    private String params;


    public int getId() {
        return id;
    }

    public void setBlock_type(Integer block_type) {
        this.block_type = block_type;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public void setHierarchy_id(Integer hierarchy_id) {
        this.hierarchy_id = hierarchy_id;
    }

    public Integer getPage_id() {
        return page_id;
    }

    public void setPage_id(Integer page_id) {
        this.page_id = page_id;
    }

    public int getBlock_type() {
        return block_type;
    }

    public void setBlock_type(int block_type) {
        this.block_type = block_type;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getHierarchy_id() {
        return hierarchy_id;
    }

    public void setHierarchy_id(int hierarchy_id) {
        this.hierarchy_id = hierarchy_id;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Integer getStyleclass() {
        return styleclass;
    }

    public void setStyleclass(Integer styleclass) {
        this.styleclass = styleclass;
    }
}

