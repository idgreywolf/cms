package com.cms.java.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blocktypes")
public class BlockTypesEntity {

    @Id
    private int id;
    private String html_tag;


    public String getHtml_tag() {
        return html_tag;
    }

    public void setHtml_tag(String html_tag) {
        this.html_tag = html_tag;
    }
}
