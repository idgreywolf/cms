package com.cms.java.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="pages")
public class PagesEntity {
    @Id
    private int id;
    private Integer user_id,style_id,parent_page;
    private String head, address;

    public int getId(){return id;}
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStyle_id() {
        return style_id;
    }

    public void setStyle_id(int style_id) {
        this.style_id = style_id;
    }

    public int getParent_page() {
        return parent_page;
    }

    public void setParent_page(int parent_page) {
        this.parent_page = parent_page;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
