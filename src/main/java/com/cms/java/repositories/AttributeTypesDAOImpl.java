package com.cms.java.repositories;

import com.cms.java.domains.AttributeTypesEntity;
import com.cms.java.repositories.interfaces.AttributeTypesDAO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class AttributeTypesDAOImpl implements AttributeTypesDAO{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addAttributeType(AttributeTypesEntity attributeTypesEntity) {

    }

    @Override
    public AttributeTypesEntity getAttributeType(int id) {
        return entityManager.find(AttributeTypesEntity.class,id);
    }
}
