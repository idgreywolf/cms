package com.cms.java.repositories;

import com.cms.java.domains.AttributeTypesEntity;
import com.cms.java.domains.BlockTypesEntity;
import com.cms.java.repositories.interfaces.BlockTypesDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class BlockTypesDAOImpl implements BlockTypesDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(BlockTypesEntity blockTypesEntity) {

    }

    @Override
    public BlockTypesEntity get(int id) {
        return entityManager.find(BlockTypesEntity.class,id);
    }
}
