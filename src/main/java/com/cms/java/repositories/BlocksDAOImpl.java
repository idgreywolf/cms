package com.cms.java.repositories;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.PagesEntity;
import com.cms.java.repositories.interfaces.BlocksDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BlocksDAOImpl implements BlocksDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(BlockEntity blockTypesEntity) {

    }

    @Override
    public List<BlockEntity> getPageTopBlocks(PagesEntity page) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BlockEntity> criteriaQuery = criteriaBuilder.createQuery(BlockEntity.class);
        Root<BlockEntity> root = criteriaQuery.from(BlockEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("page_id"), page.getId()),root.get("parent_id").isNull());
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("hierarchy_id")));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<BlockEntity> getPageUnderBlocks(BlockEntity blocks) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BlockEntity> criteriaQuery = criteriaBuilder.createQuery(BlockEntity.class);
        Root<BlockEntity> root = criteriaQuery.from(BlockEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("parent_id"),blocks.getId()));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("hierarchy_id")));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


    @Override
    public BlockEntity get(int id) {
        return entityManager.find(BlockEntity.class,id);
    }


}
