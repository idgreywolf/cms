package com.cms.java.repositories;

import com.cms.java.domains.ClassesAttributesEntity;
import com.cms.java.domains.StyleClassesEntity;
import com.cms.java.repositories.interfaces.ClassesAttributesDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ClassesAttributesDAOImpl implements ClassesAttributesDAO{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(ClassesAttributesEntity blockTypesEntity) {

    }

    @Override
    public ClassesAttributesEntity get(int id) {
        return entityManager.find(ClassesAttributesEntity.class,id);
    }

    @Override
    public List<ClassesAttributesEntity> getClassAttributes(int classId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ClassesAttributesEntity> criteriaQuery = criteriaBuilder.createQuery(ClassesAttributesEntity.class);
        Root<ClassesAttributesEntity> root = criteriaQuery.from(ClassesAttributesEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("class_id"), classId));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


}
