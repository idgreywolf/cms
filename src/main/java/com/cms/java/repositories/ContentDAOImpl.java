package com.cms.java.repositories;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.ContentEntity;
import com.cms.java.repositories.interfaces.ContentDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ContentDAOImpl implements ContentDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(ContentEntity blockTypesEntity) {

    }

    @Override
    public List<ContentEntity> getByBlock(BlockEntity block) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContentEntity> criteriaQuery = criteriaBuilder.createQuery(ContentEntity.class);
        Root<ContentEntity> root = criteriaQuery.from(ContentEntity.class);
        List<Predicate> predicates=new ArrayList<Predicate>();
        predicates.add(criteriaBuilder.equal(root.get("block_id"), block.getId()));
        criteriaQuery.select(root);
        criteriaQuery.where(predicates.toArray(new Predicate[0]));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}