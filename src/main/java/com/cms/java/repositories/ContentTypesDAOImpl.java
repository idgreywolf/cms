package com.cms.java.repositories;

import com.cms.java.domains.ContentTypesEntity;
import com.cms.java.repositories.interfaces.ContentTypesDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ContentTypesDAOImpl implements ContentTypesDAO{@PersistenceContext
private EntityManager entityManager;

    @Override
    public void add(ContentTypesEntity blockTypesEntity) {

    }

    @Override
    public ContentTypesEntity get(int id) {
        return entityManager.find(ContentTypesEntity.class,id);
    }
}
