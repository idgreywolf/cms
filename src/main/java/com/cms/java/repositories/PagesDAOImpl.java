package com.cms.java.repositories;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.PagesEntity;
import com.cms.java.domains.UsersEntity;
import com.cms.java.repositories.interfaces.PagesDAO;
import com.cms.java.repositories.interfaces.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class PagesDAOImpl implements PagesDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    UsersDAO usersDAO;

    @Override
    public void add(PagesEntity blockTypesEntity) {

    }

    @Override
    public PagesEntity get(String user, String address) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PagesEntity> criteriaQuery = criteriaBuilder.createQuery(PagesEntity.class);
        Root<PagesEntity> root = criteriaQuery.from(PagesEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("address"), address), criteriaBuilder.equal(root.get("user_id"), usersDAO.getByName(user).getId()));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public List<PagesEntity> getAllUserPages(UsersEntity usersEntity) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PagesEntity> criteriaQuery = criteriaBuilder.createQuery(PagesEntity.class);
        Root<PagesEntity> root = criteriaQuery.from(PagesEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("user_id"), usersEntity.getId()));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


}
