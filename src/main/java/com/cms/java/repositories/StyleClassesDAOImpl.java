package com.cms.java.repositories;

import com.cms.java.domains.StyleClassesEntity;
import com.cms.java.repositories.interfaces.StyleClassesDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class StyleClassesDAOImpl implements StyleClassesDAO {


    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void add(StyleClassesEntity blockTypesEntity) {

    }

    @Override
    public StyleClassesEntity get(int id) {
        return entityManager.find(StyleClassesEntity.class,id);
    }

    @Override
    public List<StyleClassesEntity> getAllStyleClasses(int classId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<StyleClassesEntity> criteriaQuery = criteriaBuilder.createQuery(StyleClassesEntity.class);
        Root<StyleClassesEntity> root = criteriaQuery.from(StyleClassesEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("style_id"), classId));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


}
