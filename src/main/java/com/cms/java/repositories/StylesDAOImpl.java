package com.cms.java.repositories;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.ClassesAttributesEntity;
import com.cms.java.domains.StyleClassesEntity;
import com.cms.java.domains.StylesEntity;
import com.cms.java.repositories.interfaces.AttributeTypesDAO;
import com.cms.java.repositories.interfaces.ClassesAttributesDAO;
import com.cms.java.repositories.interfaces.StyleClassesDAO;
import com.cms.java.repositories.interfaces.StylesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class StylesDAOImpl implements StylesDAO {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private StyleClassesDAO styleClassesDAO;
    @Autowired
    private ClassesAttributesDAO classesAttributesDAO;
    @Autowired
    private AttributeTypesDAO attributeTypesDAO;


    @Override
    public void add(StylesEntity blockTypesEntity) {

    }

    @Override
    public String getFullStyle(int id) {
        String style="<style>\n";
        List<StyleClassesEntity> listStyleClasses=styleClassesDAO.getAllStyleClasses(id);
        for (StyleClassesEntity styleClass:listStyleClasses){
            style+=styleClass.getClass_name()+"{\n";
            List<ClassesAttributesEntity> classAttributes=classesAttributesDAO.getClassAttributes(styleClass.getId());
            for (ClassesAttributesEntity classAttribute:classAttributes){
                style+=attributeTypesDAO.getAttributeType(classAttribute.getType_id()).getName()+":"+classAttribute.getValue()+";\n";
            }
            style+="}\n";
        }

        style+="</style>";
        return style;
    }

    @Override
    public StylesEntity get(int id) {
        return entityManager.find(StylesEntity.class,id);
    }
}
