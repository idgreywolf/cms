package com.cms.java.repositories;

import com.cms.java.repositories.interfaces.UniversalDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
public  class UniversalDAOImpl<T> implements UniversalDAO <T>{
    private Class entityClass;

    public UniversalDAOImpl(Class<T> entityClass) {
        this.entityClass=entityClass;
    }
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(T entity) {

    }

    @Override
    public T get(int id) {
        T res=(T)entityManager.find(entityClass,id);
        return  res;
    }
}
