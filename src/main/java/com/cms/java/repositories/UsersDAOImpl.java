package com.cms.java.repositories;

import com.cms.java.domains.PagesEntity;
import com.cms.java.domains.UsersEntity;
import com.cms.java.repositories.interfaces.UsersDAO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UsersDAOImpl implements UsersDAO {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void add(UsersEntity blockTypesEntity) {

    }

    @Override
    public UsersEntity get(int id) {
        return entityManager.find(UsersEntity.class, id);
    }

    @Override
    public List<UsersEntity> getAllUsers() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsersEntity> criteriaQuery = criteriaBuilder.createQuery(UsersEntity.class);
        Root<UsersEntity> root = criteriaQuery.from(UsersEntity.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public UsersEntity getByName(String name) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsersEntity> criteriaQuery = criteriaBuilder.createQuery(UsersEntity.class);
        Root<UsersEntity> root = criteriaQuery.from(UsersEntity.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("name"), name));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

}
