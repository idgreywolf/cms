package com.cms.java.repositories.interfaces;

import com.cms.java.domains.AttributeTypesEntity;
import org.springframework.stereotype.Service;

public interface AttributeTypesDAO {
    void addAttributeType(AttributeTypesEntity attributeTypesEntity);
    AttributeTypesEntity getAttributeType(int id);
}
