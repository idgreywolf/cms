package com.cms.java.repositories.interfaces;

import com.cms.java.domains.AttributeTypesEntity;
import com.cms.java.domains.BlockTypesEntity;
import org.springframework.stereotype.Service;

public interface BlockTypesDAO {
    void add(BlockTypesEntity blockTypesEntity);
    BlockTypesEntity get(int id);
}
