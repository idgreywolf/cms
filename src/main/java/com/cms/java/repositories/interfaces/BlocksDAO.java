package com.cms.java.repositories.interfaces;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.PagesEntity;

import java.util.List;

public interface BlocksDAO {
    void add(BlockEntity blockTypesEntity);
    List<BlockEntity>  getPageTopBlocks (PagesEntity page);
    List<BlockEntity>  getPageUnderBlocks (BlockEntity blocks);

    BlockEntity get(int id);
}
