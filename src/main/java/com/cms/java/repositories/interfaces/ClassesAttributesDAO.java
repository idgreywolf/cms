package com.cms.java.repositories.interfaces;

import com.cms.java.domains.ClassesAttributesEntity;

import java.util.List;

public interface ClassesAttributesDAO {
    void add(ClassesAttributesEntity blockTypesEntity);
    ClassesAttributesEntity get(int id);
    List<ClassesAttributesEntity> getClassAttributes(int classID);
}
