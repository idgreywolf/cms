package com.cms.java.repositories.interfaces;

import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.ContentEntity;

import java.util.List;

public interface ContentDAO {
    void add(ContentEntity blockTypesEntity);
    List<ContentEntity> getByBlock(BlockEntity block);
}
