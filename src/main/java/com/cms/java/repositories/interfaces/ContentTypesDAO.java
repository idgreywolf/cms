package com.cms.java.repositories.interfaces;

import com.cms.java.domains.ContentTypesEntity;

public interface ContentTypesDAO {
    void add(ContentTypesEntity blockTypesEntity);
    ContentTypesEntity get(int id);
}
