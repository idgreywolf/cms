package com.cms.java.repositories.interfaces;

import com.cms.java.domains.ContentEntity;
import com.cms.java.domains.PagesEntity;
import com.cms.java.domains.UsersEntity;

import java.util.List;

public interface PagesDAO {
    void add(PagesEntity blockTypesEntity);
    PagesEntity get(String user,String address);
    List<PagesEntity> getAllUserPages(UsersEntity usersEntity);
}
