package com.cms.java.repositories.interfaces;

import com.cms.java.domains.PagesEntity;
import com.cms.java.domains.StyleClassesEntity;

import java.util.List;

public interface StyleClassesDAO {
    void add(StyleClassesEntity blockTypesEntity);
    StyleClassesEntity get(int id);
    List<StyleClassesEntity> getAllStyleClasses(int classId);
}
