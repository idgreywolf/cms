package com.cms.java.repositories.interfaces;

import com.cms.java.domains.StyleClassesEntity;
import com.cms.java.domains.StylesEntity;

public interface StylesDAO {
    void add(StylesEntity blockTypesEntity);
    String getFullStyle(int id);
    StylesEntity get(int id);
}
