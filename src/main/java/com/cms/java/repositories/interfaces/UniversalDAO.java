package com.cms.java.repositories.interfaces;



public  interface UniversalDAO<T> {

     T get (int id);
     void add (T entity);
}
