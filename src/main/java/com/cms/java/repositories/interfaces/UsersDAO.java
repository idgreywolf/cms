package com.cms.java.repositories.interfaces;

import com.cms.java.domains.StyleClassesEntity;
import com.cms.java.domains.UsersEntity;

import java.util.List;

public interface UsersDAO {
    void add(UsersEntity blockTypesEntity);
    UsersEntity getByName(String name);
    UsersEntity get(int id);
    List<UsersEntity> getAllUsers();

}
