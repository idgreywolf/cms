package com.cms.java.services;


import com.cms.java.domains.BlockEntity;
import com.cms.java.domains.ContentEntity;
import com.cms.java.repositories.interfaces.BlockTypesDAO;
import com.cms.java.repositories.interfaces.BlocksDAO;
import com.cms.java.repositories.interfaces.ContentDAO;
import com.cms.java.repositories.interfaces.StyleClassesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlockConstructor {
    @Autowired
    private BlocksDAO blocksDAO;
    @Autowired
    private BlockTypesDAO blockTypesDAO;
    @Autowired
    private ContentDAO contentDAO;
    @Autowired
    private StyleClassesDAO styleClassesDAO;

    public String formBlock(BlockEntity topBlock){
        String blockHTML;

        List<BlockEntity> underBlocks=blocksDAO.getPageUnderBlocks(topBlock);
        String id=(topBlock.getStyleclass()!=null)?(" id='"+styleClassesDAO.get(topBlock.getStyleclass()).getClass_name().replace("#",""))+"'":"";
        blockHTML ="<"+blockTypesDAO.get(topBlock.getBlock_type()).getHtml_tag()+id+">";
        if (underBlocks.isEmpty()){
            for (ContentEntity content:contentDAO.getByBlock(topBlock)){
                switch (content.getContent_type()) {
                    case 1:{
                        blockHTML +=content.getContent();
                        break;
                    }
                    case 2:{
                        blockHTML +="<img src='"+content.getContent()+"'>";
                        break;
                    }
                }
            }
        }else{
            for (BlockEntity block:underBlocks){
                blockHTML+=formBlock(block);
            }
        }
        blockHTML +="</"+blockTypesDAO.get(topBlock.getBlock_type()).getHtml_tag()+">";
        return blockHTML;
    }

}
