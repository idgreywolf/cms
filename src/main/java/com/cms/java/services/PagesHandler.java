package com.cms.java.services;

import com.cms.java.domains.BlockEntity;
import com.cms.java.repositories.interfaces.BlocksDAO;
import com.cms.java.repositories.interfaces.PagesDAO;
import com.cms.java.repositories.interfaces.StylesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagesHandler {
    @Autowired
    private PagesDAO pagesDAO;
    @Autowired
    private BlocksDAO blocksDAO;
    @Autowired
    private StylesDAO stylesDAO;
    @Autowired
    private BlockConstructor blockConstructor;


    public String getHead (String user,String address){

        String head= pagesDAO.get(user,address).getHead();
        head+=stylesDAO.getFullStyle(pagesDAO.get(user,address).getStyle_id());
        return head;
    }

    public String getBody(String user,String address){
        String bodyHTML="";
        List<BlockEntity> blocks=blocksDAO.getPageTopBlocks(pagesDAO.get(user,address));
        for (BlockEntity block:blocks){
            bodyHTML+=blockConstructor.formBlock(block);
        }

        return bodyHTML;
    }
}
