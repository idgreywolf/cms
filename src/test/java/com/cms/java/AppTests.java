package com.cms.java;

import com.cms.java.domains.UsersEntity;
import com.cms.java.repositories.interfaces.PagesDAO;
import com.cms.java.repositories.interfaces.UsersDAO;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static junit.framework.TestCase.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class AppTests {


    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected DriverManagerDataSource dataSource;

    @Test
    public void dataSourceCheck() throws Exception {
       assertNotNull(dataSource);
    }

    @Autowired
    UsersDAO usersDAO;
    @Test
    public void usersTest() throws Exception{
        assertNotNull(usersDAO.getAllUsers());
    }


    @Autowired
    PagesDAO pagesDAO;

    @Test
    public void checkPages() throws Exception{

        UsersEntity usersEntity=new UsersEntity();
        usersEntity.setId(1);
        assertNotNull(pagesDAO.getAllUserPages(usersEntity));
    }
}
